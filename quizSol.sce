clear;
W_L = 20/0.18;
u_nCox = 100E-6;
u_pCox = 50E-6;
lamdaN = 0.1;
lamdaP = 0.15;

s = poly(0, "s"); 
//s = sqrt(ID)
//s^2 = ID

gm1 = sqrt(2*(W_L)*u_nCox) * s// * sqrt(ID)
gm2 = gm1; // *sqrt(ID)

gm3 = sqrt(2*(W_L)*u_pCox) * s // * sqrt(ID)
gm4 = gm3; // *sqrt(ID)

ro1 = 1/(lamdaN*s^2); // sobre ID
ro2 = ro1; // sobre ID

ro3 = 1/(lamdaP*s^2); // sobre ID
ro4 = ro3; // sobre ID

A = ro2 + (1+gm2*ro3)*ro1;
B = ro3 + (1+gm3*ro3)*ro4;
AparalelB = (A*B)/(A+B);

Av = -gm1*AparalelB;

// Si |Av| = 500 entonces:

// 500*abs(denom(Av)) = abs(numer(Av))
// 0.0000002s^2 + 0.0000004s^3 =  +1.681D-10 + 8.322D-10s + 9.998D-10s^2
//  1.681D-10 + 8.322D-10s + 0.0000002s^2 + 0.0000004s^3 = 0

p = 500*abs(denom(Av)) - abs(numer(Av))
sol = roots(p)

//- 0.4305576  
//    0.0314577  
//  - 0.0290553  

// Si s=sqrt(ID) entonces descartamos las soluciones negativas.
// Sin embargo se comprueba con los tres valores de ID y sólo con
// el segundo se cumple la condición de ganancia.



//-----------------------------------------------
// Pruebas:
//-----------------------------------------------
// Recordemos que s = sqrt(ID)
ID1 = (sol(1))^2
ID2 = (sol(2))^2
ID3 = (sol(3))^2

s_1 = ID2;

gm1_1 = sqrt(2*(W_L)*u_nCox) * sqrt(s_1)// * sqrt(ID)
gm2_1 = gm1_1; // *sqrt(ID)

gm3_1 = sqrt(2*(W_L)*u_pCox) * sqrt(s_1) // * sqrt(ID)
gm4_1 = gm3_1; // *sqrt(ID)

ro1_1 = 1/(lamdaN*s_1); // sobre ID
ro2_1 = ro1_1; // sobre ID

ro3_1 = 1/(lamdaP*s_1); // sobre ID
ro4_1 = ro3_1; // sobre ID

A_1 = ro2_1 + (1+gm2_1*ro3_1)*ro1_1;
B_1 = ro3_1 + (1+gm3_1*ro3_1)*ro4_1;
AparalelB_1 = (A_1*B_1)/(A_1+B_1);

Av_1 = -gm1_1*AparalelB_1;

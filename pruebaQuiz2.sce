clear

W_L = 113;
u_nCox = 100E-6;
u_pCox = 50E-6;
lamdaN = 0.1;
lamdaP = 0.15;

ID = 1.045E-3

gm1 = sqrt(2*(W_L)*u_nCox*ID)// * sqrt(ID)
gm2 = gm1; // *sqrt(ID)

gm3 = sqrt(2*(W_L)*u_pCox*ID) // * sqrt(ID)
gm4 = gm3; // *sqrt(ID)

ro1 = 1/(lamdaN*ID); // sobre ID
ro2 = ro1; // sobre ID

ro3 = 1/(lamdaP*ID); // sobre ID
ro4 = ro3; // sobre ID

A = ro2 + (1+gm2*ro3)*ro1;
B = ro3 + (1+gm3*ro3)*ro4;
AparalelB = (A*B)/(A+B);

Av = -gm1*AparalelB;

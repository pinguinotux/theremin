EESchema Schematic File Version 2
LIBS:theremind-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:switches
LIBS:myTransistors
LIBS:theremind-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 7 11
Title "Theremind"
Date ""
Rev "0.1"
Comp "Hijos de Pachamama"
Comment1 "jgcubidesc@unal.edu.co"
Comment2 "Johnny Cubides"
Comment3 "crpulidog@unal.edu.co"
Comment4 "Carolina Pulido "
$EndDescr
$Comp
L L Ld1
U 1 1 592383BF
P 4650 3650
F 0 "Ld1" V 4600 3650 50  0000 C CNN
F 1 "2.5mH" V 4725 3650 50  0000 C CNN
F 2 "" H 4650 3650 50  0001 C CNN
F 3 "" H 4650 3650 50  0000 C CNN
	1    4650 3650
	0    1    1    0   
$EndComp
$Comp
L L Ld2
U 1 1 592383FB
P 5200 3650
F 0 "Ld2" V 5150 3650 50  0000 C CNN
F 1 "2.5mH" V 5275 3650 50  0000 C CNN
F 2 "" H 5200 3650 50  0001 C CNN
F 3 "" H 5200 3650 50  0000 C CNN
	1    5200 3650
	0    1    1    0   
$EndComp
$Comp
L L Ld3
U 1 1 59238418
P 5750 3650
F 0 "Ld3" V 5700 3650 50  0000 C CNN
F 1 "5mH" V 5825 3650 50  0000 C CNN
F 2 "" H 5750 3650 50  0001 C CNN
F 3 "" H 5750 3650 50  0000 C CNN
	1    5750 3650
	0    1    1    0   
$EndComp
$Comp
L L Ld4
U 1 1 5923843A
P 6250 3650
F 0 "Ld4" V 6200 3650 50  0000 C CNN
F 1 "5mH" V 6325 3650 50  0000 C CNN
F 2 "" H 6250 3650 50  0001 C CNN
F 3 "" H 6250 3650 50  0000 C CNN
	1    6250 3650
	0    1    1    0   
$EndComp
$Comp
L D Dd1
U 1 1 5923848C
P 4650 3150
F 0 "Dd1" H 4650 3250 50  0000 C CNN
F 1 "1n4148" H 4650 3050 50  0000 C CNN
F 2 "" H 4650 3150 50  0001 C CNN
F 3 "" H 4650 3150 50  0000 C CNN
	1    4650 3150
	-1   0    0    1   
$EndComp
$Comp
L C Cd1
U 1 1 592384E6
P 4250 3400
F 0 "Cd1" H 4275 3500 50  0000 L CNN
F 1 "1000pF" H 4275 3300 50  0000 L CNN
F 2 "" H 4288 3250 50  0001 C CNN
F 3 "" H 4250 3400 50  0000 C CNN
	1    4250 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6100 3650 5900 3650
Wire Wire Line
	5600 3650 5350 3650
Wire Wire Line
	5050 3650 4800 3650
Wire Wire Line
	4050 3650 4500 3650
Wire Wire Line
	4250 3650 4250 3550
Wire Wire Line
	4250 3250 4250 3150
Wire Wire Line
	4050 3150 4500 3150
Wire Wire Line
	4800 3150 4950 3150
Wire Wire Line
	4950 3150 4950 3650
Connection ~ 4950 3650
$Comp
L R Rd1
U 1 1 59238621
P 3900 3150
F 0 "Rd1" V 3980 3150 50  0000 C CNN
F 1 "560k" V 3900 3150 50  0000 C CNN
F 2 "" V 3830 3150 50  0001 C CNN
F 3 "" H 3900 3150 50  0000 C CNN
	1    3900 3150
	0    1    1    0   
$EndComp
Connection ~ 4250 3150
Wire Wire Line
	3750 3150 3550 3150
Wire Wire Line
	6400 3650 6600 3650
Text HLabel 4050 3650 0    60   Input ~ 0
inOV
Text HLabel 3550 3150 0    60   Output ~ 0
outVolAnt
Connection ~ 4250 3650
Text HLabel 6600 3650 2    60   Input ~ 0
inAnt
$EndSCHEMATC

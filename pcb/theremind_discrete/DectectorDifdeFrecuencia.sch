EESchema Schematic File Version 2
LIBS:theremind-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:switches
LIBS:myTransistors
LIBS:theremind-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 13
Title "Theremind"
Date ""
Rev "0.1"
Comp "Hijos de Pachamama"
Comment1 "jgcubidesc@unal.edu.co"
Comment2 "Johnny Cubides"
Comment3 "crpulidog@unal.edu.co"
Comment4 "Carolina Pulido "
$EndDescr
$Comp
L D Dh1
U 1 1 592338B3
P 2850 3250
F 0 "Dh1" H 2850 3350 50  0000 C CNN
F 1 "1n4148" H 2850 3150 50  0000 C CNN
F 2 "Diodes_THT:D_A-405_P2.54mm_Vertical_KathodeUp" H 2850 3250 50  0001 C CNN
F 3 "" H 2850 3250 50  0000 C CNN
	1    2850 3250
	0    -1   -1   0   
$EndComp
$Comp
L R Rh1
U 1 1 592338F8
P 3300 3050
F 0 "Rh1" V 3380 3050 50  0000 C CNN
F 1 "10k" V 3300 3050 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P2.54mm_Vertical" V 3230 3050 50  0001 C CNN
F 3 "" H 3300 3050 50  0000 C CNN
	1    3300 3050
	1    0    0    -1  
$EndComp
$Comp
L R Rh2
U 1 1 5923391F
P 3300 3450
F 0 "Rh2" V 3380 3450 50  0000 C CNN
F 1 "4.7k" V 3300 3450 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P2.54mm_Vertical" V 3230 3450 50  0001 C CNN
F 3 "" H 3300 3450 50  0000 C CNN
	1    3300 3450
	1    0    0    -1  
$EndComp
$Comp
L C Ch3
U 1 1 59233944
P 3750 3450
F 0 "Ch3" H 3775 3550 50  0000 L CNN
F 1 "4700pF" H 3775 3350 50  0000 L CNN
F 2 "Capacitors_THT:C_Rect_L4.6mm_W2.0mm_P2.50mm_MKS02_FKP02" H 3788 3300 50  0001 C CNN
F 3 "" H 3750 3450 50  0000 C CNN
	1    3750 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 3200 3300 3300
Wire Wire Line
	3750 3300 3750 3250
Wire Wire Line
	3300 3250 4000 3250
Connection ~ 3300 3250
Wire Wire Line
	3300 2800 3300 2900
Wire Wire Line
	2600 2800 3300 2800
Wire Wire Line
	2850 2550 2850 3100
Wire Wire Line
	2850 3400 2850 3800
Wire Wire Line
	2850 3800 3750 3800
Wire Wire Line
	3750 3800 3750 3600
Wire Wire Line
	3300 3600 3300 3900
Connection ~ 3300 3800
$Comp
L C Ch2
U 1 1 59233B24
P 2450 2800
F 0 "Ch2" H 2475 2900 50  0000 L CNN
F 1 "22pF" H 2475 2700 50  0000 L CNN
F 2 "Capacitors_THT:C_Rect_L4.6mm_W2.0mm_P2.50mm_MKS02_FKP02" H 2488 2650 50  0001 C CNN
F 3 "" H 2450 2800 50  0000 C CNN
	1    2450 2800
	0    1    1    0   
$EndComp
$Comp
L C Ch1
U 1 1 59233B7F
P 2450 2550
F 0 "Ch1" H 2475 2650 50  0000 L CNN
F 1 "22pF" H 2475 2450 50  0000 L CNN
F 2 "Capacitors_THT:C_Rect_L4.6mm_W2.0mm_P2.50mm_MKS02_FKP02" H 2488 2400 50  0001 C CNN
F 3 "" H 2450 2550 50  0000 C CNN
	1    2450 2550
	0    1    1    0   
$EndComp
Connection ~ 2850 2800
Wire Wire Line
	2600 2550 2850 2550
Wire Wire Line
	2300 2800 2150 2800
Wire Wire Line
	2300 2550 2150 2550
Text HLabel 2150 2550 0    60   Input ~ 0
inA
Text HLabel 2150 2800 0    60   Input ~ 0
inB
Text HLabel 4000 3250 2    60   Output ~ 0
out
Connection ~ 3750 3250
$Comp
L GND #PWR08
U 1 1 5923554B
P 3300 3900
F 0 "#PWR08" H 3300 3650 50  0001 C CNN
F 1 "GND" H 3300 3750 50  0000 C CNN
F 2 "" H 3300 3900 50  0000 C CNN
F 3 "" H 3300 3900 50  0000 C CNN
	1    3300 3900
	1    0    0    -1  
$EndComp
$EndSCHEMATC

EESchema Schematic File Version 2
LIBS:theremind-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:switches
LIBS:myTransistors
LIBS:theremind-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 13
Title "Theremind"
Date ""
Rev "0.1"
Comp "Hijos de Pachamama"
Comment1 "jgcubidesc@unal.edu.co"
Comment2 "Johnny Cubides"
Comment3 "crpulidog@unal.edu.co"
Comment4 "Carolina Pulido "
$EndDescr
$Comp
L L La1
U 1 1 59164F72
P 5200 3950
F 0 "La1" V 5150 3950 50  0000 C CNN
F 1 "10mH" V 5275 3950 50  0000 C CNN
F 2 "" H 5200 3950 50  0001 C CNN
F 3 "" H 5200 3950 50  0000 C CNN
	1    5200 3950
	0    1    1    0   
$EndComp
Wire Wire Line
	4950 3950 5050 3950
Text HLabel 6750 3950 2    60   Output ~ 0
AntennaToneOut
Wire Wire Line
	5550 3950 5350 3950
Text HLabel 4950 3950 0    60   Input ~ 0
AntennaTono
Text Notes 3950 4300 0    60   ~ 0
Observar si comercialmente se encuentra una de 40mH,\nen caso contrario hacer un arreglo con 4 inductacias de 10 mH
$Comp
L L La2
U 1 1 59243474
P 5700 3950
F 0 "La2" V 5650 3950 50  0000 C CNN
F 1 "10mH" V 5775 3950 50  0000 C CNN
F 2 "" H 5700 3950 50  0001 C CNN
F 3 "" H 5700 3950 50  0000 C CNN
	1    5700 3950
	0    1    1    0   
$EndComp
$Comp
L L La3
U 1 1 592434A6
P 6150 3950
F 0 "La3" V 6100 3950 50  0000 C CNN
F 1 "10mH" V 6225 3950 50  0000 C CNN
F 2 "" H 6150 3950 50  0001 C CNN
F 3 "" H 6150 3950 50  0000 C CNN
	1    6150 3950
	0    1    1    0   
$EndComp
$Comp
L L La4
U 1 1 592434DC
P 6550 3950
F 0 "La4" V 6500 3950 50  0000 C CNN
F 1 "10mH" V 6625 3950 50  0000 C CNN
F 2 "" H 6550 3950 50  0001 C CNN
F 3 "" H 6550 3950 50  0000 C CNN
	1    6550 3950
	0    1    1    0   
$EndComp
Wire Wire Line
	6750 3950 6700 3950
Wire Wire Line
	6400 3950 6300 3950
Wire Wire Line
	6000 3950 5850 3950
$EndSCHEMATC
